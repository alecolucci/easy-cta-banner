��          �       �       �      �        >   	     H     X     a  f   g     �     �     �  
   �            �       �     �  I   �       
         +  l   2     �     �     �     �     �     �   Add CTA CTA Easily insert CTA banners into posts and pages via shortcodes. Easy CTA Banner Edit CTA Image Link to where the CTA should redirect the user. Enter the complete URL, eg: https://www.blog.com/ebook New CTA No CTAs found No CTAs in the trash Search CTA URL View CTA Project-Id-Version: Easy CTA Banner
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-06 16:58+0000
PO-Revision-Date: 2018-11-06 17:32+0000
Last-Translator: alexandre <alexandrecolucci@gmail.com>
Language-Team: Português do Brasil
Language: pt_BR
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/ Adicionar CTA CTA Insira facilmente banners CTA em posts e páginas por meio de shortcodes. Easy CTA Banner Editar CTA Imagem Link para onde o CTA deve redirecionar o usuário. Digite o URL completo, por ex: https://www.blog.com/ebook Novo CTA Nenhum CTA encontrado Nenhum CTA na lixeira Pesquisar CTA URL Ver CTA 