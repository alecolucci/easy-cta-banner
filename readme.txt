=== Easy CTA Banner ===
Contributors: alecolucci
Tags: cta, banner, marketing
Donate link: https://bitbucket.org/alecolucci/easy-cta-banner/
Requires at least: 4.0
Tested up to: 4.9
Requires PHP: 5.6
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily insert CTA banners into posts and pages via shortcodes.

== Description ==
Easily insert CTA banners into posts and pages via shortcodes.

== Installation ==
1. Upload entire \"easy-cta-banner\" folder to the \"/wp-content/plugins/\" directory.
2. Activate the plugin through the \"Plugins\" menu in WordPress.
3. After publish your CTA, place the shortcode [cta-banner id=\"POSTID\"] in any page or post, with the proper post ID.

== Frequently Asked Questions ==
= If do not display the banner? =
Check if the ID is correct.

== Screenshots ==
1. CTA Banner panel screenshot screenshot-1.(png|jpg|jpeg|gif).

== Changelog ==
= 1.0 =
* Initial release.

== Upgrade Notice ==
= 1.0 =
No upgrades.