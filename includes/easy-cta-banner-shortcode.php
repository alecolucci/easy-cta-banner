<?php
/**
 * Easy CTA Banner
 *
 * @package   Easy_Cta_Banner
 * @license   GPLv2+
 */

/**
 * Register shortcode.
 *
 * @package Easy_Cta_Banner
 */
class Easy_Cta_Banner_Shortcode {

	public function init() {
		add_shortcode( 'cta-banner', array( $this, 'display_cta' ) );
	}

	/**
	* Processes shortcode cta-banner
	*
	* @param array $atts the attributes from the shortcode
	*
	* @return mixed $output
	*/
	public function display_cta( $atts ) {

		$attributes = shortcode_atts( array( 'id' => null ), $atts );
		$post_id = $attributes['id'];

		$cta = get_post($post_id);

		$output = '';

		if (!empty($cta) && $cta->post_type == 'cta') {
			$output .= '<div class="cta-banner"><a href='. $cta->cta_url .'>';
			$output .= get_the_post_thumbnail($post_id, 'cta_banner');
			$output .= '</a></div>';
		}

		return $output;

	}

}