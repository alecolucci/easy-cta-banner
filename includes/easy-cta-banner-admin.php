<?php
/**
 * Easy CTA Banner
 *
 * @package   Easy_Cta_Banner
 * @license   GPLv2+
 */

/**
 * Register post types.
 *
 * @package Easy_Cta_Banner
 */
class Easy_Cta_Banner_Admin {

	protected $registration_handler;

	public function __construct( $registration_handler ) {
		$this->registration_handler = $registration_handler;
	}

	public function init() {

		// Add thumbnail support for this post type
		add_theme_support( 'post-thumbnails', array( $this->registration_handler->post_type ) );

		// Add thumbnails to column view
		add_filter( 'manage_edit-' . $this->registration_handler->post_type . '_columns', array( $this, 'add_image_column' ), 10, 1 );
		add_action( 'manage_' . $this->registration_handler->post_type . '_posts_custom_column', array( $this, 'display_image' ), 10, 1 );

	}

	/**
	 * Add columns to post type list screen.
	 *
	 * @link http://wptheming.com/2010/07/column-edit-pages/
	 *
	 * @param array $columns columns.
	 *
	 * @return array columns.
	 */
	public function add_image_column( $columns ) {
		$column_thumbnail = array( 'thumbnail' => __( 'Image', 'easy-cta-banner' ) );
		return array_slice( $columns, 0, 2, true ) + $column_thumbnail + array_slice( $columns, 1, null, true );
	}

	/**
	 * Custom column callback
	 *
	 * @param string $column Column ID.
	 */
	public function display_image( $column ) {
		switch ( $column ) {
			case 'thumbnail':
				echo get_the_post_thumbnail( get_the_ID(), array( 35, 35 ) );
				break;
		}
	}

}