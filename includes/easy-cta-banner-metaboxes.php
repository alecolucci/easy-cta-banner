<?php
/**
 * Easy CTA Banner
 *
 * @package   Easy_Cta_Banner
 * @license   GPLv2+
 */

/**
 * Register metaboxes.
 *
 * @package Easy_Cta_Banner
 */
class Easy_Cta_Banner_Metaboxes {

	public function init() {
		add_action( 'add_meta_boxes', array( $this, 'cta_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ),  10, 2 );
	}

	/**
	 * Register the metaboxes to be used for the cpt
	 *
	 * @since 1.0.0
	 */
	public function cta_meta_boxes() {
		add_meta_box(
			'cta_fields',
			__( 'URL', 'easy-cta-banner' ),
			array( $this, 'render_meta_boxes' ),
				'cta',
				'normal',
				'high'
		);
	}

   /**
	* The HTML for the fields
	*
	* @since 1.0.0
	*/
	public function render_meta_boxes( $post ) {

		$meta = get_post_custom( $post->ID );
		$title = ! isset( $meta['cta_url'][0] ) ? '' : $meta['cta_url'][0];

		wp_nonce_field( basename( __FILE__ ), 'cta_fields' ); ?>

		<table class="form-table">

			<tr>
				<td>
					<input type="text" name="cta_url" class="regular-text" value="<?php echo $title; ?>">
					<p class="description"><?php _e( 'Link to where the CTA should redirect the user. Enter the complete URL, eg: https://www.blog.com/ebook', 'easy-cta-banner' ); ?></p>
				</td>
			</tr>

		</table>

	<?php }

   /**
	* Save metaboxes
	*
	* @since 1.0.0
	*/
	public function save_meta_boxes( $post_id ) {

		global $post;

		// Verify nonce
		if ( !isset( $_POST['cta_fields'] ) || !wp_verify_nonce( $_POST['cta_fields'], basename(__FILE__) ) ) {
			return $post_id;
		}

		// Check Autosave
		if ( ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || ( defined('DOING_AJAX') && DOING_AJAX ) || isset($_REQUEST['bulk_edit']) ) {
			return $post_id;
		}

		// Don't save if only a revision
		if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
			return $post_id;
		}

		// Check permissions
		if ( !current_user_can( 'edit_post', $post->ID ) ) {
			return $post_id;
		}

		$meta['cta_url'] = ( isset( $_POST['cta_url'] ) ? esc_textarea( $_POST['cta_url'] ) : '' );

		foreach ( $meta as $key => $value ) {
			update_post_meta( $post->ID, $key, $value );
		}
	}

}