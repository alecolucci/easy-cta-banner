<?php
/**
 * Easy CTA Banner
 *
 * @package   Easy_Cta_Banner
 * @license   GPLv2+
 */

/**
 * Register post types and taxonomies.
 *
 * @package Easy_Cta_Banner
 */
class Easy_Cta_Banner_Registrations {

	public $post_type = 'cta';

	public function init() {
		// Add the Easy CTA Banner
		add_action( 'init', array( $this, 'register' ) );
	}

	/**
	 * Initiate registrations of post type.
	 *
	 * @uses Easy_Cta_Banner_Registrations::register_post_type()
	 */
	public function register() {
		$this->register_post_type();
	}

	/**
	 * Register the custom post type.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_post_type
	 */
	protected function register_post_type() {
		$labels = array(
			'name'               => __( 'CTA', 'easy-cta-banner' ),
			'singular_name'      => __( 'CTA', 'easy-cta-banner' ),
			'add_new'            => __( 'Add CTA', 'easy-cta-banner' ),
			'add_new_item'       => __( 'Add CTA', 'easy-cta-banner' ),
			'edit_item'          => __( 'Edit CTA', 'easy-cta-banner' ),
			'new_item'           => __( 'New CTA', 'easy-cta-banner' ),
			'view_item'          => __( 'View CTA', 'easy-cta-banner' ),
			'search_items'       => __( 'Search CTA', 'easy-cta-banner' ),
			'not_found'          => __( 'No CTAs found', 'easy-cta-banner' ),
			'not_found_in_trash' => __( 'No CTAs in the trash', 'easy-cta-banner' ),
		);

		$supports = array(
			'title',
			'thumbnail',
			'revisions',
		);

		$args = array(
			'labels'          => $labels,
			'supports'        => $supports,
			'public'          => true,
			'capability_type' => 'post',
			'menu_position'   => 30,
			'menu_icon'       => 'dashicons-format-image',
		);

		$args = apply_filters( 'Easy_Cta_Banner_args', $args );

		register_post_type( $this->post_type, $args );
	}

}