<?php
/**
 * Easy CTA Banner
 *
 * @package   Easy_Cta_Banner
 * @license   GPLv2+
 *
 * @wordpress-plugin
 * Plugin Name: Easy CTA Banner
 * Plugin URI:  https://bitbucket.org/alecolucci/easy-cta-banner/
 * Description: Easily insert CTA banners into posts and pages via shortcodes.
 * Version:     1.0.0
 * Author:      Alexandre Colucci
 * Author URI:  https://bitbucket.org/alecolucci/easy-cta-banner/
 * Text Domain: easy-cta-banner
 * License:     GPLv2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Domain Path: /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Required files for registering the post type and shortcode.
require plugin_dir_path( __FILE__ ) . 'includes/easy-cta-banner.php';
require plugin_dir_path( __FILE__ ) . 'includes/easy-cta-banner-registrations.php';
require plugin_dir_path( __FILE__ ) . 'includes/easy-cta-banner-metaboxes.php';
require plugin_dir_path( __FILE__ ) . 'includes/easy-cta-banner-shortcode.php';

// Instantiate registration class, so we can add it as a dependency to main plugin class.
$easy_cta_banner_registrations = new Easy_Cta_Banner_Registrations;

// Instantiate main plugin file, so activation callback does not need to be static.
$easy_cta_banner = new Easy_Cta_Banner( $easy_cta_banner_registrations );

// Register callback that is fired when the plugin is activated.
register_activation_hook( __FILE__, array( $easy_cta_banner, 'activate' ) );

// Initialize registrations for post-activation requests.
$easy_cta_banner_registrations->init();

// Initialize metaboxes
$easy_cta_banner_metaboxes = new Easy_Cta_Banner_Metaboxes;
$easy_cta_banner_metaboxes->init();

// Initialize Shortcode
$easy_cta_banner_shortcode = new Easy_Cta_Banner_Shortcode;
$easy_cta_banner_shortcode->init();

/**
 * Adds styling to the list for the cpt
 * 
 */
if ( is_admin() ) {

	require plugin_dir_path( __FILE__ ) . 'includes/easy-cta-banner-admin.php';

	$easy_cta_banner_admin = new Easy_Cta_Banner_Admin( $easy_cta_banner_registrations );
	$easy_cta_banner_admin->init();

}